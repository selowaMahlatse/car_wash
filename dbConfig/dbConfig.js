const { Client } = require("pg"); //Helps to connedct to postgres database.

//const con_string = "postgres://postgres:12345678@localhost:5432/postgres"
const con_string = "postgres://postgres:postgres@localhost:5432/postgres"; //Home

const client = new Client({
    connectionString: con_string
});

client.connect(function(err) {
    if(err){
        throw err;
    } 

    console.log("Car Wash database successfully connected");
});

module.exports = client;