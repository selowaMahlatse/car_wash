const express = require("express");
const bodyParser = require("body-parser");
const multer = require("multer"); //Image upload.
const path = require("path"); //Image upload.
const bcrypt = require("bcrypt"); //Password encryption.
const jwt = require("jsonwebtoken");
const cors = require("cors");

const corsOptions = {
    origin: 'http://localhost:4302'
}

const maxAge = 1 * 24 * 60 * 60;

const createToken = (phone) => {
    return jwt.sign({ phone }, 'secreteToken', {
        'expiresIn': maxAge
    })
}


const storage = multer.diskStorage({
    destination: './images/car_wash',
    filename: (req, file, cb) => {
        return cb(null, `${file.fieldname}_${Date.now()}${path.extname(file.originalname)}`);
    }
});

const upload = multer({
    storage: storage
});

const app = express();

app.use(cors(corsOptions));
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

const port = process.env.PORT || 4303;

app.listen(port, () => {
    console.log(`Car Wash server up and running on port ${port}`);
});

const postgres = require("./dbConfig/dbConfig");
const { resolve } = require("path");

app.get("/get", (req, res) => {
    return new Promise((resolve, reject) => {
        const sql = "SELECT * FROM car_wash_schema.users";

        postgres.query(sql)
        .then((data)=> {
            res.status(200).json({
                message: 'User List',
                user: data.rows,
            });

            resolve(data);
        }).catch((error) => {
            res.status(500).json({
                message: 'Failed to get',
                error: error,
                status: false
            });

            reject(error);
        })
    });
    
});
function encryptPassword(pass){
    const salt = bcrypt.genSalt(10);
    const hashPassword = bcrypt.hash(pass, salt);

    return hashPassword; 
}

app.post("/register", (req, res) => {
    return new Promise((resolve, reject) => {

        let phone = "'" + req.body.phone + "'";

        const sqlQ = `SELECT * from car_wash_schema.fn_check_phone(${phone})`;
        postgres.query(sqlQ)
        .then((results) => {
            const phoneFound = results.rows[0].fn_check_phone;
        if(phoneFound){
            res.status(400).json({
                message: 'Phone number already registered.'
            });
        }
        else{
            const hashedPassword = encryptPassword(req.body.password)
            console.log(hashedPassword);
            const values = [req.body.name, req.body.surname, req.body.phone, hashedPassword];
            const sql = "INSERT INTO car_wash_schema.users(name, surname, phone, password) VALUES($1, $2, $3, $4)";
    
            postgres.query(sql, values)
            .then((data) => {
                res.status(201).json({
                    message: 'User successfully created',
                    userData: data
                });
    
                resolve(data);
    
            }).catch((error) => {
                res.status(500).json({
                    message: error.message,
                    error: error,
                    status: false
                });
    
                reject(error);
            });
        }
        })
    });
});


app.post("/login", (req, res) => {
    return new Promise((resolve, reject) => {
        const phone = "'" + req.body.phone + "'";
        const password = "'" + req.body.password + "'";

        const sqlQ = `SELECT * FROM car_wash_schema.login(${phone}, ${password})`;

        postgres.query(sqlQ)
        .then((data) => {
            let loggedIn = data.rows[0].login;
            console.log(loggedIn);

            if(loggedIn){
                var token = createToken(phone);
                res.cookie('jwt', token, { httpOnly: true, maxAge: maxAge * 1000});

                res.status(200).json({
                    message: 'Logged In',
                    user: data.rows,
                    jwt: token
                });
            }
            else
            {
                res.status(400).json({
                    message: 'Wrong credentials',
                });
            }

            resolve(data);
        })
        .catch((error) => {
            res.status(500).json({
                message: error,
                error: error,
                status: false
            });

            reject(error);
        });
    });
});


app.use("/profile", express.static('images/car_wash'));
app.post("/car_wash_details", upload.single('profile'), (req, res) => {
    return new Promise((resolve, reject) => {
        console.log(req.file.path);
        const img = req.file.path;
        const sql = "INSERT INTO test(car_wash_name, car_wash_address, phone, images) VALUES($1, $2, $3, $4)";
        const values = [req.body.car_wash_name, req.body.car_wash_address, req.body.phone, img];

        postgres.query(sql, values)
        .then((data) => {
            res.status(200).json({
                message: 'Car Wash successfully created',
                car_wash_details: data,
                image_url: `http://localhost:4301/profile/${req.file.filename}`
            });

            resolve(data);

        }).catch((error) => {
            res.status(500).json({
                message: 'Failed',
                error: error,
                status: false
            });

            reject(error);
        });
    });
});
